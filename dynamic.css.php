/* Base font size */
html {
  font-size: <?php print theme_get_setting('base_font_size'); ?>;
}

/* Layout width */
body.two-sidebars #navigation,
body.two-sidebars #header,
body.two-sidebars #main-columns {
  width: <?php print theme_get_setting('layout_3_width'); ?>;
  min-width:<?php print theme_get_setting('layout_3_min_width'); ?>;
  max-width: <?php print theme_get_setting('layout_3_max_width'); ?>;
}
body.one-sidebar #navigation,
body.one-sidebar #header,
body.one-sidebar #main-columns {
  width: <?php print theme_get_setting('layout_2_width'); ?>;
  min-width: <?php print theme_get_setting('layout_2_min_width'); ?>;
  max-width: <?php print theme_get_setting('layout_2_max_width'); ?>;
}
body.no-sidebars #navigation,
body.no-sidebars #header,
body.no-sidebars #main-columns {
  width: <?php print theme_get_setting('layout_1_width'); ?>;
  min-width: <?php print theme_get_setting('layout_1_min_width'); ?>;
  max-width: <?php print theme_get_setting('layout_1_max_width'); ?>;
}

/**
 * Color schemes
 */
<?php

$color = theme_get_setting('color_scheme');

$colors = array(
  'default' => array(
    'body' => '#f4f2f3 url(/'. path_to_theme().'/images/bg_1.jpg) no-repeat left top',
    'base' => '#ffffff',
    'background' => '#057ed1',
    'text' => '#2e2e2e',
    'title' => '#27638C',
    'link' => '#086782',
    'linkhover' => '#e25401',
    'linkunderline' => '#cfdde5',
    'sitename' => '#ffffff',
    'slogan' => '#314f64',
    'navigation' => '#035c94',
    'navigationhover' => '#057ed1',
    'navigationactive' => '#057ed1',
    'tab' => '#f5f4f3',
    'blockbg' => '#035c94',
    'blocktitle' => '#ffffff',
    'block' => '#2e2e2e',
    'blockact' => '#086782',
    'border' => '#bbbbbb',
    'borderstrong' => '#c4c4c4',
    'fieldset' => '#fbfbfb',
    'fieldsetborder' => '#e1e1e2',
  ),
  
  'sea' => array(
    'body' => '#1182bf url(/'. path_to_theme().'/images/bg_2.jpg) no-repeat left top',
    'base' => '#ffffff',
    'background' => '#057ed1',
    'text' => '#2e2e2e',
    'title' => '#27638C',
    'link' => '#086782',
    'linkhover' => '#e25401',
    'linkunderline' => '#cfdde5',
    'sitename' => '#ffffff',
    'slogan' => '#035c94',
    'navigation' => '#035c94',
    'navigationhover' => '#057ed1',
    'navigationactive' => '#057ed1',
    'tab' => '#f5f4f3',
    'blockbg' => '#035c94',
    'blocktitle' => '#ffffff',
    'block' => '#2e2e2e',
    'blockact' => '#ffffff',
    'border' => '#035c94',
    'borderstrong' => '#c4c4c4',
    'fieldset' => '#fbfbfb',
    'fieldsetborder' => '#e1e1e2',
  ),
  'beach' => array(
    'body' => '#e5d8b8 url(/'. path_to_theme().'/images/bg_3.jpg) no-repeat center top',
    'base' => '#ffffff',
    'background' => '#057ed1',
    'text' => '#2e2e2e',
    'title' => '#27638C',
    'link' => '#086782',
    'linkhover' => '#e25401',
    'linkunderline' => '#cfdde5',
    'sitename' => '#ffffff',
    'slogan' => '#035c94',
    'navigation' => '#035c94',
    'navigationhover' => '#057ed1',
    'navigationactive' => '#057ed1',
    'tab' => '#f5f4f3',
    'blockbg' => '#2b9ce6',
    'blocktitle' => '#ffffff',
    'block' => '#2e2e2e',
    'blockact' => '#086782',
    'border' => '#cac2ad',
    'borderstrong' => '#c4c4c4',
    'fieldset' => '#fbfbfb',
    'fieldsetborder' => '#e1e1e2',
  ),
  'garden' => array(
    'body' => '#badcd0 url(/'. path_to_theme().'/images/bg_4.jpg) no-repeat center top',
    'base' => '#ffffff',
    'background' => '#368572',
    'text' => '#2e2e2e',
    'title' => '#27638C',
    'link' => '#086782',
    'linkhover' => '#e25401',
    'linkunderline' => '#cfdde5',
    'sitename' => '#ffffff',
    'slogan' => '#cccccc',
    'navigation' => '#0f4e45',
    'navigationhover' => '#368572',
    'navigationactive' => '#368572',
    'tab' => '#f5f4f3',
    'blockbg' => '#368572',
    'blocktitle' => '#ffffff',
    'block' => '#2e2e2e',
    'blockact' => '#0f4e45',
    'border' => '#6fb5a4',
    'borderstrong' => '#c4c4c4',
    'fieldset' => '#fbfbfb',
    'fieldsetborder' => '#e1e1e2',
  ),  
  'city' => array(
    'body' => '#e0e3cd url(/'. path_to_theme().'/images/bg_5.jpg) no-repeat center top',
    'base' => '#ffffff',
    'background' => '#ff6600',
    'text' => '#2e2e2e',
    'title' => '#27638C',
    'link' => '#086782',
    'linkhover' => '#e25401',
    'linkunderline' => '#cfdde5',
    'sitename' => '#fd2e2e',
    'slogan' => '#000000',
    'navigation' => '#eeb07d',
    'navigationhover' => '#ff6600',
    'navigationactive' => '#ff6600',
    'tab' => '#f5f4f3',
    'blockbg' => '#eeb07d',
    'blocktitle' => '#ffffff',
    'block' => '#2e2e2e',
    'blockact' => '#086782',
    'border' => '#bbbbbb',
    'borderstrong' => '#c4c4c4',
    'fieldset' => '#fbfbfb',
    'fieldsetborder' => '#e1e1e2',
  ),    
  'sunset' => array(
    'body' => '#020204 url(/'. path_to_theme().'/images/bg_6.jpg) no-repeat right top',
    'base' => '#ffffff',
    'background' => '#035c94',
    'text' => '#2e2e2e',
    'title' => '#27638C',
    'link' => '#086782',
    'linkhover' => '#e25401',
    'linkunderline' => '#cfdde5',
    'sitename' => '#ffffff',
    'slogan' => '#000000',
    'navigation' => '#000000',
    'navigationhover' => '#035c94',
    'navigationactive' => '#035c94',
    'tab' => '#f5f4f3',
    'blockbg' => '#035c94',
    'blocktitle' => '#ffffff',
    'block' => '#cccccc',
    'blockact' => '#ffffff',
    'border' => '#000000',
    'borderstrong' => '#c4c4c4',
    'fieldset' => '#fbfbfb',
    'fieldsetborder' => '#e1e1e2',
  ),
  'lava' => array(
    'body' => '#e57e3b url(/'. path_to_theme().'/images/bg_7.jpg) no-repeat left top',
    'base' => '#ffffff',
    'background' => '#e57e3b',
    'text' => '#2e2e2e',
    'title' => '#642b0d',
    'link' => '#086782',
    'linkhover' => '#e25401',
    'linkunderline' => '#cfdde5',
    'sitename' => '#ffffff',
    'slogan' => '#fac19c',
    'navigation' => '#642b0d',
    'navigationhover' => '#e57e3b',
    'navigationactive' => '#e57e3b',
    'tab' => '#f5f4f3',
    'blockbg' => '#642b0d',
    'blocktitle' => '#ffffff',
    'block' => '#2e2e2e',
    'blockact' => '#ffffff',
    'border' => '#894216',
    'borderstrong' => '#c4c4c4',
    'fieldset' => '#fbfbfb',
    'fieldsetborder' => '#e1e1e2',
  ),
);
?>

/* Background */
body {
  background: <?php print $colors[$color]['body']; ?>;
}

#site-name a {
  color: <?php print $colors[$color]['sitename']; ?>;
}

#navigation > ul > li.active-trail {
  background-color: <?php print $colors[$color]['background']; ?>;
}
/* Text (black) */
body,
#navigation li.active a,
.tabs ul.tabs li a,
pre,
code,
samp,
var,
table.update tr,
table.system-status-report tr {
  color: <?php print $colors[$color]['text']; ?>;
}

h1.page-title,
.node h2.node-title,
.node h1.node-title a,
.node h2.node-title a,
.comment h3.title a {
  color: <?php print $colors[$color]['title']; ?>;
}

#site-name a::-moz-selection {
  background-color: <?php print $colors[$color]['text']; ?>;
}
#site-name a::selection {
  background-color: <?php print $colors[$color]['text']; ?>;
}
.node-title a::-moz-selection {
  background-color: <?php print $colors[$color]['text']; ?>;
}
.node-title a::selection {
  background-color: <?php print $colors[$color]['text']; ?>;
}
/* Link (blue) */
a,
a.active,
li a.active {
  color: <?php print $colors[$color]['link']; ?>;
}
legend {
  background-color: <?php print $colors[$color]['link']; ?>;
}
/* Link hovered (orange) */
a:hover,
a:focus,
a.active:hover,
a.active:focus,
li a.active:hover,
li a.active:focus {
  color: <?php print $colors[$color]['linkhover']; ?>;
  border-bottom-color: <?php print $colors[$color]['linkhover']; ?>;
}
.node h1.node-title a:hover,
.node h2.node-title a:hover {
  color: <?php print $colors[$color]['linkhover']; ?>;
}
/* Link underline (light blue) */
a {
  border-bottom: 1px solid <?php print $colors[$color]['linkunderline']; ?>;
}

/* Link (block) */
.block .content {
  color: <?php print $colors[$color]['block']; ?>;
}

.block .content a {
  color: <?php print $colors[$color]['blockact']; ?>;
  font-weight: bold;
}



/* Navigation (black) */
#navigation-wrapper {
  background-color: <?php print $colors[$color]['navigation']; ?>;
}
/* Navigation hovered (orange) */
#navigation li a:hover,
#navigation li a:active,
#navigation li a:focus {
  background-color: <?php print $colors[$color]['navigationhover']; ?>;
}

/* Navigation active (orange) */
#navigation > ul > li.active-trail > a {
  background-color: <?php print $colors[$color]['navigationactive']; ?>;
}

.block h2 {
  background-color: <?php print $colors[$color]['blockbg']; ?>;
}

/* Slogan (orange) */
#site-slogan {
  color: <?php print $colors[$color]['slogan']; ?>;
}
#site-slogan::selection {
  background-color: <?php print $colors[$color]['navigationhover']; ?>;
}
#site-slogan::-moz-selection {
  background-color: <?php print $colors[$color]['navigationhover']; ?>;
}
.poll .bar .foreground {
  background-color: <?php print $colors[$color]['navigationhover']; ?>;
}
/* Border (gray) */
#page {
  border: 1px solid <?php print $colors[$color]['border']; ?>;
}
/* Border strong (dark gray) */
#sidebar-first input,
#sidebar-second input {
  border: 1px solid <?php print $colors[$color]['borderstrong']; ?>;
}
.tabs ul.tabs li a {
  border: 1px solid <?php print $colors[$color]['borderstrong']; ?>;
}
/* Tab */
.tabs ul.tabs li a {
  background-color: <?php print $colors[$color]['tab']; ?>;
}
ul.vertical-tabs-list li a {
  background-color: <?php print $colors[$color]['tab']; ?>;
}
/* Block title (green) */
.block h2,
h2 {
  color: <?php print $colors[$color]['blocktitle']; ?>;
}
.block h2::selection {
  color: #fff;
  background-color: <?php print $colors[$color]['blocktitle']; ?>;
}
.block h2::-moz-selection {
  background-color: <?php print $colors[$color]['blocktitle']; ?>;
  color: #fff;
}
/* Fieldset (gray) */
fieldset {
  background-color: <?php print $colors[$color]['fieldset']; ?>;
}
/* Fieldset border (dark gray) */
fieldset {
  border: 1px solid <?php print $colors[$color]['fieldsetborder']; ?>;
}

